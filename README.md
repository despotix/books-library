##### Run application with performance measurement


You need to install docker, docker-compose, Node.JS and then run:


```
docker-compose up -d mysql; npm i; npm start
```

Also you may run through `docker-compose up -d` but you wouldn't be able to see the performance bars.


##### Scalability


( how long does it take to index all the content )


It depends of your CPU number of cores.
Also you may scale by sending an `entry` event to another endpoint
instead of sending it to another worker-thread in current implementation.

My local results

```
 - cpu#1 [=====================================================================] 100% > 101 rps > 15393 of 15393
 - cpu#2 [=====================================================================] 100% > 94 rps > 15392 of 15392
 - cpu#3 [=====================================================================] 100% > 108 rps > 15392 of 15392
 - cpu#4 [=====================================================================] 100% > 262 rps > 15392 of 15392
```

##### Reliability
( does all the information process correctly? )


Some data could not be parsed as expected. `null` is used for this cases.



##### Querying the dataset

( how should the database be configured to search for specific fields quickly? )

Indexes are already added to the initial script `./docker/files/install_db.sql`

