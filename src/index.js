const cluster = require('cluster');

if (cluster.isMaster) { ( async () => {

	const tar = require('tar-stream');
	const fs = require('fs');
	const bz2 = require('unbzip2-stream');
	const axios = require('axios');
	const numCPUs = require('os').cpus().length;
	const Multiprogress = require('multi-progress');
	// const numCPUs = 2;

	const BooksDatabase = require('./lib/db');

	const db = new BooksDatabase({
		host: process.env.MYSQL_HOST || 'localhost',
		user: process.env.MYSQL_USER || 'root',
		password: process.env.MYSQL_PASSWORD || 'root',
		database: process.env.MYSQL_DATABASE || 'library',
	});

	const cpu_stat = {};
	let is_done = false;
	let total_count_got = 0;
	let total_count_sent = 0;

	const multi_bar = Multiprogress(process.stdout);
	const download_bar = multi_bar.newBar('  downloading [:bar] :percent :etas',
		{total: 1000}
	);

	const cpu_bar = {};
	await db.connect();
	for (let i = 0; i < numCPUs; i++) {
		let worker = cluster.fork();
		cpu_bar[worker.id] = multi_bar.newBar(
			` - cpu#${worker.id} [:bar] :percent > :rps rps > :count_got of :count_sent`,
			{total: 1000}
		);
		cpu_stat[worker.id] = {
			count_sent: 0,
			count_got: 0,
			one_sec_ts: Date.now(),
			one_sec_cnt: 0,
			rps: 0
		};
	}

	const fn = process.env.FILE_NAME ||
		'./docker/files/rdf-files.tar';
	const url = process.env.URL ||
		'https://www.gutenberg.org/cache/epub/feeds/rdf-files.tar.bz2';

	const extract = tar.extract();
	let iterator = 1;

	extract
	.on('entry', (header, stream, next) => {
		let data = '';

		stream.on('data', (chunk) => {
			data += chunk;
		});

		stream.on('end', () => {
			next();
			let worker = cluster.workers[iterator];
			worker.send({header, data});
			cpu_stat[worker.id].count_sent++;
			total_count_sent++;
			iterator = (iterator === numCPUs) ? 1 : iterator + 1;
		});

		stream.resume();
	})
	.on('finish', () => {
		is_done = true;
	});

	fs.promises.stat(fn, 'r')
	.then(() =>
		fs.createReadStream(fn).pipe(extract)
	)
	.catch(() =>
		axios({
			method: 'get',
			url: url,
			responseType: 'stream'
		})
		.then((response) => {
			let down_bytes = 0;
			let down_size = parseInt(response.headers["content-length"]);
			const write_stream = fs.createWriteStream(fn);
			const download_stream = response.data
			.on('data', (chunk) => {
				down_bytes += chunk.length;
				download_bar.update(down_bytes / down_size);
			})
			.pipe(bz2());

			download_stream.pipe(extract);

			download_stream.pipe(write_stream);
		})
	);

	console.time('TOTAL');
	cluster.on('message', async (worker, message) => {
		//console.timeEnd('PROC' + message.header.name);
		total_count_got++;
		cpu_stat[worker.id].count_got++;

		cpu_stat[worker.id].one_sec_cnt++;
		if (Date.now() - cpu_stat[worker.id].one_sec_ts >= 1000) {
			cpu_stat[worker.id].rps = cpu_stat[worker.id].one_sec_cnt;
			cpu_stat[worker.id].one_sec_ts = Date.now();
			cpu_stat[worker.id].one_sec_cnt = 0;
		}

		cpu_bar[worker.id].update(
			cpu_stat[worker.id].count_got / cpu_stat[worker.id].count_sent,
			cpu_stat[worker.id]
		);

		if(message.result) {
			await db.add_bulk(message.result, 100);
		}
		if (is_done && (total_count_got === total_count_sent)) {
			console.log('\n');
			console.timeEnd('TOTAL');
			// stop workers
			for (let i in cluster.workers) {
				cluster.workers[i].kill();
			}
			await db.add_bulk(null, 1, true);
			await db.disconnect();
			process.exit(0);
		}
	});

})() } else {
	const Book = require('./lib/book');

	process.on('message', ({header, data}) => {
		let result = null;
		let error = null;
		Book.parse(header, data)
		.then(obj => {
			//console.log(obj);
			result = obj;
		})
		.catch(e => {
			error = e;
			console.error(e);
		})
		.finally(() => {
			process.send({header, result, error});
		});
	});
}


