const mysql = require('mysql');

class BooksBatabase {
	constructor(config) {
		this._connected = false;
		this.db = mysql.createConnection(config);
		this._insert_arr = [];
	}


	async connect() {
		if (this._connected) {
			return this._connected;
		}
		this._connected = await new Promise((resolve, reject) =>
			this.db.connect(err =>
				err ? reject(err) : resolve(true)
			));
		return this._connected;
	}

	disconnect() {
		return this.db.end();
	}

	add_many(_book_arr) {
		let book_arr = _book_arr.filter(v => !!v);
		book_arr.forEach(v =>
			v.publication_date = new Date(v.publication_date)
		);
		if (!book_arr.length) return Promise.resolve([]);
		const params = book_arr.map(() => '(?, ?, ?, ?, ?, ?, ?)').join(', ');
		let values = [];
		book_arr.forEach((v) => values = values.concat(Object.values(v)));
		const query = `insert into books (${Object.keys(book_arr[0]).join(',')}) values ${params}`;
		return new Promise((resolve, reject) =>
			this.db.query(query, values, (err, results, fields) =>
				err ? reject(err) : resolve([results, fields])
			)
		);
	}

	add_bulk(book, limit = 100, end = false) {
		if (book) this._insert_arr.push(book);
		if (!end && (this._insert_arr.length < limit)) {
			return null;
		}
		return this.add_many(
			this._insert_arr.splice(0, limit)
		);
	}

}

module.exports = BooksBatabase;
