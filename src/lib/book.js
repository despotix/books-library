const {parseStringPromise} = require('xml2js');

class Book {
	static title_get(link) {
		return link["dcterms:title"]["0"];
	}

	static author_get(link) {
		return JSON.stringify(link["dcterms:creator"].map(creator => {
			let ret = null;
			try {
				ret = creator["pgterms:agent"]["0"]["pgterms:name"]["0"]
			} catch (e) {
				//console.warn(e, creator);
			}
			return ret;
		}).filter(x => !!x));
	}

	static publication_date(link) {
		return link["dcterms:issued"]["0"]._;
	}

	static language_get(link) {
		let ret = null;
		try {
			ret = link["dcterms:language"]["0"]["rdf:Description"]["0"]["rdf:value"]["0"]._;
		} catch {
			//console.warn(e, creator);
		}
		return ret;
	}

	static subjects_get(link) {
		return JSON.stringify(link["dcterms:subject"].map(sbj => {
			let ret = null;
			try {
				ret = sbj["rdf:Description"]["0"]["rdf:value"]["0"];
			} catch (e) {
				//console.warn(e, sbj);
			}
			return ret;
		}).filter(x => !!x));
	}

	static license_rights_get(link) {
		return link["dcterms:rights"]["0"];
	}

	static publisher_get(link) {
		return link["dcterms:publisher"]["0"];
	}

	static async parse(header, data) {
		let [ebook] = data
		.replace(/[\n|\r]+/g, ' ')
		.match(/<pgterms:ebook\s*.*?<\/pgterms:ebook>/g);
		const json = await parseStringPromise(ebook);
		let link = json["pgterms:ebook"];

		let def = [
			"dcterms:title",
			"dcterms:creator",
			"pgterms:agent",
			"dcterms:publisher",
			"dcterms:issued",
			"dcterms:language",
			"dcterms:subject",
			"dcterms:rights"
		];

		for (let k of def) {
			link[k] = link[k] || [];
		}

		const result = {
			title: Book.title_get(link) || null,
			author: Book.author_get(link) || null,
			publisher: Book.publisher_get(link) || null,
			publication_date: Book.publication_date(link) || null,
			language: Book.language_get(link) || null,
			subjects: Book.subjects_get(link) || null,
			license_rights: Book.license_rights_get(link) || null
		};

		return result;
	}
}

module.exports = Book;
