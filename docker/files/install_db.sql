USE library;

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE TABLE IF NOT EXISTS `books` (
  `id` bigint(5) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(200),
  `author` varchar(200),
  `publisher` tinytext,
  `publication_date` date,
  `language` tinytext,
  `subjects` text,
  `license_rights` tinytext,
  PRIMARY KEY (`id`),
  KEY `title` (`title`),
  KEY `author` (`author`),
  KEY `publication_date` (`publication_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

